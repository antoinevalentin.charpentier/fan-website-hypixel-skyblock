import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Secret from './components/pages/Secret';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Bazaar from './components/pages/Bazaar';
import Secret2 from './components/pages/Secret2';


function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Secret} />
        <Route path='/bazaar' component={Bazaar} />
        <Route path='/secret2' component={Secret2}/>
      </Switch>
    </Router>
  );
}

export default App;