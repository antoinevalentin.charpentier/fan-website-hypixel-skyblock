import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';

function Navbar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <>
      <nav className='navbar'>
        <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
          SKYBLOCK
        </Link>
        <div className='menu-icon' onClick={handleClick}>
          <p className={click ? 'hamburger hidden' : 'hamburger'} >≡</p>
        </div>
        <ul className={click ? 'nav-menu active' : 'nav-menu'}>
          <li className='nav-item'>
            <Link to='/' className='nav-links' onClick={closeMobileMenu}>
              Secret
            </Link>
          </li>

          <li className='nav-item'>
            <Link
              to='/bazaar'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              Bazaar
            </Link>
          </li>

         
        </ul>
      </nav>
    </>
  );
}

export default Navbar;