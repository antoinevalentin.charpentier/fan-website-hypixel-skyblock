//size : 1=1x1, 2=1x2, 3=1x3, 4=1x4, 5=2x2, 6=L 
const ROOMS_SECRETS = [
    //salle de 1x1
    {sizeRoom: 1, nbSecret: 1, nameRoom: "arrow-trap", urlVideo: "https://www.youtube.com/embed/-4pFAK_gd-E", nbSreenSecretBySecret:[3]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "banners", urlVideo: "https://www.youtube.com/embed/KbVeCUvx93Q", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "blue-skulls", urlVideo: "https://www.youtube.com/embed/zf6DaxSaxws", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "cage", urlVideo: "NULL", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "carpets", urlVideo: "NULL", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "cell", urlVideo: "https://www.youtube.com/embed/G2EgUJnk4Lw", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "duncan", urlVideo: "https://www.youtube.com/embed/9bQCg3i5k1w", nbSreenSecretBySecret:[2]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "golden-oasis", urlVideo: "https://www.youtube.com/embed/kDqxp-veI94", nbSreenSecretBySecret:[6]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "hanging-vines", urlVideo: "NULL", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "jumping-skulls", urlVideo: "https://www.youtube.com/embed/eQIJ4QkRhjM", nbSreenSecretBySecret:[2]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "leaves", urlVideo: "https://www.youtube.com/embed/YtYvXhlV6mQ", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "locked-away", urlVideo: "https://www.youtube.com/embed/bEzTstbkwqE", nbSreenSecretBySecret:[2]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "mirror", urlVideo: "https://www.youtube.com/embed/dHfgk9sQStU", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "multicolored", urlVideo: "https://www.youtube.com/embed/-nF1nloirZE", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "mural", urlVideo: "https://www.youtube.com/embed/CBAF2yEUocQ", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "mushroom", urlVideo: "https://www.youtube.com/embed/KKWOlUZAkoY", nbSreenSecretBySecret:[2]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "pillars", urlVideo: "NULL", nbSreenSecretBySecret:[3]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "prison-cell", urlVideo: "https://www.youtube.com/embed/GtEve3Zrhbg", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "sanctuary", urlVideo: "NULL", nbSreenSecretBySecret:[2]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "sand-dragon", urlVideo: "NULL", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "silvers-sword", urlVideo: "NULL", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "sloth", urlVideo: "https://www.youtube.com/embed/fRhsTaWRFWU", nbSreenSecretBySecret:[1]},
    {sizeRoom: 1, nbSecret: 1, nameRoom: "steps", urlVideo: "https://www.youtube.com/embed/7D63KTdbQtg", nbSreenSecretBySecret:[1]}, 
    {sizeRoom: 1, nbSecret: 1, nameRoom: "three-floors", urlVideo: "NULL", nbSreenSecretBySecret:[4]},

    {sizeRoom: 1, nbSecret: 2, nameRoom: "andesite", urlVideo: "https://www.youtube.com/embed/ZMfAGwd5YkM", nbSreenSecretBySecret:[2,2]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "beams", urlVideo: "https://www.youtube.com/embed/qsMBTle0Csk", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "big-red-flag", urlVideo: "https://www.youtube.com/embed/hpQZEg6w_0s", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "chains", urlVideo: "NULL", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "cobble-wall-pillar", urlVideo: "https://www.youtube.com/embed/zwHFYg2iFvw", nbSreenSecretBySecret:[2,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "dip", urlVideo: "https://www.youtube.com/embed/736_brvJ2jY", nbSreenSecretBySecret:[2,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "dome", urlVideo: "https://www.youtube.com/embed/dLdzITnXg7Y", nbSreenSecretBySecret:[1,2]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "drop", urlVideo: "https://www.youtube.com/embed/esgz8ewO04s", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "granite", urlVideo: "https://www.youtube.com/embed/NxOTNCmJ2h0", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "painting", urlVideo: "https://www.youtube.com/embed/aI5yWXhrRGQ", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "perch", urlVideo: "https://www.youtube.com/embed/wtLvJV5WZA8", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "quad-lava", urlVideo: "https://www.youtube.com/embed/3Gk52cz2Glg", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "scaffolding", urlVideo: "https://www.youtube.com/embed/dWMl4zqNL6M", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "slabs", urlVideo: "https://www.youtube.com/embed/66H0jm4EFrI", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "small-stairs", urlVideo: "https://www.youtube.com/embed/KyaVpelZxNI", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "tombstone", urlVideo: "NULL", nbSreenSecretBySecret:[2,2]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "water", urlVideo: "https://www.youtube.com/embed/PS-rYcSPW20", nbSreenSecretBySecret:[2,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "waterfall", urlVideo: "https://www.youtube.com/embed/9dGNaXcknjc", nbSreenSecretBySecret:[1,1]},
    {sizeRoom: 1, nbSecret: 2, nameRoom: "stone-window", urlVideo: "NULL", nbSreenSecretBySecret:[1,1]},

    {sizeRoom: 1, nbSecret: 3, nameRoom: "black-flag", urlVideo: "https://www.youtube.com/embed/gcISkgdniGw&feature=youtu.be&ab_channel=ActuallySilver", nbSreenSecretBySecret:[1,1,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "dueces", urlVideo: "https://www.youtube.com/embed/qFCZSeu5-Hc", nbSreenSecretBySecret:[1,1,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "knight", urlVideo: "https://www.youtube.com/embed/TIwuccMEPNk", nbSreenSecretBySecret:[3,3,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "lava-pool", urlVideo: "NULL", nbSreenSecretBySecret:[1,1,2]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "lava-skull", urlVideo: "NULL", nbSreenSecretBySecret:[1,2,2]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "lots-of-floors", urlVideo: "https://www.youtube.com/embed/mJAZ8J18tFE", nbSreenSecretBySecret:[1,1,2]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "mini-rail-track", urlVideo: "NULL", nbSreenSecretBySecret:[1,1,2]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "overgrown", urlVideo: "https://www.youtube.com/embed/6nuTzhCl6Pg", nbSreenSecretBySecret:[1,3,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "redstone-key", urlVideo: "https://www.youtube.com/embed/NzqH5YYCKqQ", nbSreenSecretBySecret:[5,1,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "sarcophagus", urlVideo: "https://www.youtube.com/embed/yJwAGRO0bhk", nbSreenSecretBySecret:[1,1,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "spikes", urlVideo: "https://www.youtube.com/embed/P6NOF839aC8", nbSreenSecretBySecret:[1,1,1]},
    {sizeRoom: 1, nbSecret: 3, nameRoom: "temple", urlVideo: "https://www.youtube.com/embed/mJAZ8J18tFE", nbSreenSecretBySecret:[1,1,2]},
    
    {sizeRoom: 1, nbSecret: 4, nameRoom: "trinity", urlVideo: "NULL", nbSreenSecretBySecret:[1,1,1,1]},
    {sizeRoom: 1, nbSecret: 4, nameRoom: "logs", urlVideo: "https://www.youtube.com/embed/gMgyNsSk3aY", nbSreenSecretBySecret:[2,2,1,1]},
    {sizeRoom: 1, nbSecret: 4, nameRoom: "raccoon", urlVideo: "https://www.youtube.com/embed/8zY5dxwdEY0", nbSreenSecretBySecret:[1,1,1,1]},

    

    //salle de 1x2
    {sizeRoom: 2, nbSecret: 1, nameRoom: "gold", urlVideo: "https://www.youtube.com/embed/uf1vPZo4Wqk", nbSreenSecretBySecret:[1]},

    {sizeRoom: 2, nbSecret: 2, nameRoom: "skull", urlVideo: "https://www.youtube.com/embed/0-745_UuuIs", nbSreenSecretBySecret:[1,1]},

    {sizeRoom: 2, nbSecret: 3, nameRoom: "archway", urlVideo: "https://www.youtube.com/embed/1-9wWoB96Bk", nbSreenSecretBySecret:[1,1,1]},
    {sizeRoom: 2, nbSecret: 3, nameRoom: "redstone-warrior", urlVideo: "https://www.youtube.com/embed/kCDaWcsBYR0&ab_channel=ActuallySilver", nbSreenSecretBySecret:[1,1,2]},

    {sizeRoom: 2, nbSecret: 4, nameRoom: "balcony", urlVideo: "https://www.youtube.com/embed/n-tUj8zgY80", nbSreenSecretBySecret:[1,1,2,2]},
    {sizeRoom: 2, nbSecret: 4, nameRoom: "mage", urlVideo: "https://www.youtube.com/embed/Q1ZhUhrFMFE", nbSreenSecretBySecret:[1,1,1,1]},

    {sizeRoom: 2, nbSecret: 5, nameRoom: "crypt", urlVideo: "https://www.youtube.com/embed/9ymHUP8z9FU", nbSreenSecretBySecret:[1,1,1,2,2]},
    {sizeRoom: 2, nbSecret: 5, nameRoom: "doors", urlVideo: "https://www.youtube.com/embed/gDCjfNQw658", nbSreenSecretBySecret:[1,1,4,4,1]},
    {sizeRoom: 2, nbSecret: 5, nameRoom: "pedestal", urlVideo: "https://www.youtube.com/embed/9FodYDnX_Ks", nbSreenSecretBySecret:[1,1,1,1,1]},
    {sizeRoom: 2, nbSecret: 5, nameRoom: "purple-flags", urlVideo: "https://www.youtube.com/embed/FR7PqWNa2Ew", nbSreenSecretBySecret:[1,1,3,1,1]},

    {sizeRoom: 2, nbSecret: 6, nameRoom: "bridges", urlVideo: "https://www.youtube.com/embed/CoB3rDIVF4g", nbSreenSecretBySecret:[2,1,1,2,2,2]},
    {sizeRoom: 2, nbSecret: 6, nameRoom: "pressure-plate", urlVideo: "https://www.youtube.com/embed/XSuzizi7VB4", nbSreenSecretBySecret:[1,1,1,2,2,2]},



    //salle de 1x3
    {sizeRoom: 3, nbSecret: 4, nameRoom: "diagonal", urlVideo: "https://www.youtube.com/embed/VAYNz9g9QNA", nbSreenSecretBySecret:[1,1,1,1]},
    {sizeRoom: 3, nbSecret: 4, nameRoom: "red-blue", urlVideo: "https://www.youtube.com/embed/qzzQVMehoag", nbSreenSecretBySecret:[2,1,1,1]},
    {sizeRoom: 3, nbSecret: 4, nameRoom: "wizard", urlVideo: "https://www.youtube.com/embed/vyiqhy6sLPM", nbSreenSecretBySecret:[1,1,1,3]},

    {sizeRoom: 3, nbSecret: 6, nameRoom: "catwalk", urlVideo: "https://www.youtube.com/embed/5kiWzGtsk0Q", nbSreenSecretBySecret:[1,1,1,1,1,1]},
    {sizeRoom: 3, nbSecret: 6, nameRoom: "deathmite", urlVideo: "https://www.youtube.com/embed/4EzBM-jSIis", nbSreenSecretBySecret:[1,1,1,2,1,1]},
    {sizeRoom: 3, nbSecret: 6, nameRoom: "gravel", urlVideo: "https://www.youtube.com/embed/FWebOYN2Y9s", nbSreenSecretBySecret:[2,1,1,1,1,1]},



    //salle de 1x4
    {sizeRoom: 4, nbSecret: 3, nameRoom: "hallway", urlVideo: "https://www.youtube.com/embed/CvkfdhyqAIs", nbSreenSecretBySecret:[1,1,1]},

    {sizeRoom: 4, nbSecret: 4, nameRoom: "mossy", urlVideo: "https://www.youtube.com/embed/IHGAVRIqvE4", nbSreenSecretBySecret:[1,2,1,1]},

    {sizeRoom: 4, nbSecret: 5, nameRoom: "pit", urlVideo: "https://www.youtube.com/embed/QDska86GpGQ", nbSreenSecretBySecret:[1,1,1,1,1]},

    {sizeRoom: 4, nbSecret: 7, nameRoom: "quartz-knight", urlVideo: "https://www.youtube.com/embed/Svrb0azih9A", nbSreenSecretBySecret:[1,1,1,1,1,1,1]},



    //salle de 2x2
    {sizeRoom: 5, nbSecret: 4, nameRoom: "stairs", urlVideo: "https://www.youtube.com/embed/rf3MPDbaVt8", nbSreenSecretBySecret:[1,1,1,1]},

    {sizeRoom: 5, nbSecret: 5, nameRoom: "buttons", urlVideo: "https://www.youtube.com/embed/NOXbttb-Zok", nbSreenSecretBySecret:[1,1,2,2,1]},
    {sizeRoom: 5, nbSecret: 5, nameRoom: "museum", urlVideo: "https://www.youtube.com/embed/e4ux6oWaZ38", nbSreenSecretBySecret:[2,1,1,1,1]},

    {sizeRoom: 5, nbSecret: 6, nameRoom: "atlas", urlVideo: "https://www.youtube.com/embed/8TLKyY6dfkw", nbSreenSecretBySecret:[2,3,1,1,1,1]},
    {sizeRoom: 5, nbSecret: 6, nameRoom: "super-tall", urlVideo: "https://www.youtube.com/embed/YfSmzehX2Z0", nbSreenSecretBySecret:[1,2,2,3,3,3]},

    {sizeRoom: 5, nbSecret: 7, nameRoom: "flags", urlVideo: "https://www.youtube.com/embed/cbM2-dgejnQ", nbSreenSecretBySecret:[1,1,1,1,1,1,1]},

    {sizeRoom: 5, nbSecret: 8, nameRoom: "cathedral", urlVideo: "https://www.youtube.com/embed/_XwaS9Z4jQk", nbSreenSecretBySecret:[1,1,1,1,1,1,1,1]},
    
    {sizeRoom: 5, nbSecret: 9, nameRoom: "rail-track", urlVideo: "https://www.youtube.com/embed/Kr1tB7oHgZo", nbSreenSecretBySecret:[1,1,1,1,1,1,2,1,1]},



    //salle en L
    {sizeRoom: 6, nbSecret: 4, nameRoom: "dino-dig-site", urlVideo: "https://www.youtube.com/embed/vO0cXZ9MMl8", nbSreenSecretBySecret:[1,2,1,2]},
    {sizeRoom: 6, nbSecret: 4, nameRoom: "withermancers", urlVideo: "https://www.youtube.com/embed/RkLNORNTl20", nbSreenSecretBySecret:[3,1,1,2]},

    {sizeRoom: 6, nbSecret: 5, nameRoom: "chambers", urlVideo: "https://www.youtube.com/embed/XeUPsiiy-vA", nbSreenSecretBySecret:[2,1,2,1,1]},
    {sizeRoom: 6, nbSecret: 5, nameRoom: "market", urlVideo: "https://www.youtube.com/embed/3XyVTPW6G10", nbSreenSecretBySecret:[1,1,1,1,1]},

    {sizeRoom: 6, nbSecret: 6, nameRoom: "lava-ravine", urlVideo: "https://www.youtube.com/embed/a2cmjDJImH0", nbSreenSecretBySecret:[1,1,1,2,2,1]},

    {sizeRoom: 6, nbSecret: 7, nameRoom: "melon", urlVideo: "https://www.youtube.com/embed/Uw1LQy59bus", nbSreenSecretBySecret:[1,1,1,1,1,1,1]},
    {sizeRoom: 6, nbSecret: 7, nameRoom: "well", urlVideo: "https://www.youtube.com/embed/s0JM8oPoiRQ", nbSreenSecretBySecret:[1,1,1,1,1,1,1]},

    {sizeRoom: 6, nbSecret: 9, nameRoom: "spider", urlVideo: "https://www.youtube.com/embed/i2dZPywdfvg", nbSreenSecretBySecret:[1,1,1,1,1,1,1,2,1]}
];

//extension de chaque image
const EXTENSION = ".png";

//nombre de secrets associé à chaque room
const NB_OF_SECRET_PER_ROOMS = [
    [1,2,3,4],//normalement il y a aussi des rooms avec 0 secret donc useless de rechercher les secrets de ces pièces
    [1,2,3,4,5,6],
    [4,6],
    [3,4,5,7],
    [4,5,6,7,8,9],
    [4,5,6,7,9]
];

const NAME = ['1x1','1x2','1x3','1x4','2x2','L shape'];

export {ROOMS_SECRETS, NAME, NB_OF_SECRET_PER_ROOMS, EXTENSION};