import React from 'react';

import {NAME, EXTENSION} from '../ListRooms.js';
import {images} from '../ImagesVideosDico.js';

import '../../App.css';
import './SecretGallery.css';
import './SecretInfoVid.css';

function RenderVideo({url}){
  if(url !== "NULL"){
    return <><p className="title_secret">Vidéo</p>
      <div className="container_iframe">
        <div>
          <iframe src={url} frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title={url}></iframe>   
        </div>
      </div>
      
    </>
  }else{
    return <><p className="title_secret">Vidéo</p>
      <p>Vidéo non disponible</p>
    </>;
  }
}

function InfoRoom({secret_room}){
  return <>
    <p className="title_secret">Informations de la salle</p>
    <table>
      <tr>
        <th>Nom</th>
        <td>{secret_room.nameRoom}</td>
      </tr>
      <tr>
        <th>Taille</th>
        <td>{NAME[secret_room.sizeRoom-1]}</td>
      </tr>
      <tr>
        <th>Nombre de secret</th>
        <td>{secret_room.nbSecret}</td>
      </tr>
    </table>
  </>
}

function ScreenRow({path}){
  return <div className="image">
    <span>
      <img src={images[path].default} alt={path}/>
    </span>
  </div>
}

function DisplayOneSecret(props){
  const secret_room = props.secretRoom;
  const i = props.i;
  let rows = []

  for(let j=1;j<=secret_room.nbSreenSecretBySecret[i-1];j++){
    let path = "secrets/"+NAME[secret_room.sizeRoom-1]+"/"+secret_room.nbSecret+"/"+secret_room.nameRoom+"/secret/"+i+"/"+j+EXTENSION;
    rows.push(<ScreenRow key={"room="+secret_room.pathScreen+"i="+i+"j="+j} path={path}/>)
  }
  return <>
    <div>
      <p key={"titleCategorySecret-room="+secret_room.nameRoom+"i="+i} className="title_secret">Secret n°{i}</p>
      <div className="gallery">
        {rows}
      </div>
    </div> 
  </>
}

function ScreenSecretsList(props){
  const secret_room = props.secretRoom;
  let rows = []

  for(let i=1;i<=secret_room.nbSecret;i++){
    rows.push(<DisplayOneSecret secretRoom={secret_room} i={i}/>)
  } 

  return <>
        <div>
          {rows}
        </div>
  </>
}


export default function Secret2(props){
  let secret_room = props.location.secret2Props.room.secret;
  return (
    <>
      <InfoRoom secret_room={secret_room}/>
      <RenderVideo url={secret_room.urlVideo}/>
      <ScreenSecretsList secretRoom={secret_room}/>
    </>
  );
}