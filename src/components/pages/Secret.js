import React from 'react';
import { Link } from 'react-router-dom';

import {ROOMS_SECRETS, NAME, NB_OF_SECRET_PER_ROOMS, EXTENSION} from '../ListRooms.js';
import {images} from '../ImagesVideosDico.js'

import '../../App.css';
import './SecretGallery.css';
import './SecretSearchBar.css';


console.log({images})
function SecretRow({secret}){
    let path = "secrets/"+NAME[secret.sizeRoom-1]+"/"+secret.nbSecret+"/"+secret.nameRoom+"/presentation/1"+EXTENSION;
    console.log({path});
    return <div className="image">
        <span><Link to={{
            pathname:'/secret2',
            secret2Props:{
                room:{secret}
            }
        }}><img src={images[path].default} alt={path}/></Link></span>
        
    </div>
}

function SecretTable({secrets,sizeRoom,nbSecret}){
    const rows = []
    secrets.forEach(secret => {
        if(sizeRoom == secret.sizeRoom && nbSecret == secret.nbSecret){
            rows.push(<SecretRow key={"secrets/"+NAME[secret.sizeRoom]+"/"+secret.nbSecret+"/"+secret.nameRoom+"/"} secret={secret}/>)
        }
    })

    return <div>
            <p className="title_research">Le résultat de la recherche pour une salle de taille {NAME[sizeRoom-1]} comportant {nbSecret} secrets est :</p>
            <div className="gallery">{rows}</div>
        </div>
}

class SecretSearchBar extends React.Component{

    constructor(props){
        super(props)
        this.handleFilterSizeRoomChange = this.handleFilterSizeRoomChange.bind(this)
        this.handleFilterNbSecretChange = this.handleFilterNbSecretChange.bind(this)
    }

    handleFilterSizeRoomChange(e){
        this.props.onFilterSizeRoomChange(e.target.value)
    }

    handleFilterNbSecretChange(e){
        this.props.onFilterNbSecretChange(e.target.value)
    }

    render(){
        const {sizeRoom, nbSecret} = this.props

        return <>
            <div className="secret_filter">
                <p>Filtrer les secrets</p>
                <form>
                    <div className="filter_type">
                        <h5>Taille de la salle</h5>
                        {NAME.map((element,index)=>{
                            return (
                                <label key={index+1}>{element}
                                    <input type="radio" value={index+1} checked={sizeRoom == index+1} onChange={this.handleFilterSizeRoomChange}/>
                                </label>
                            );
                        })}
                    </div>

                    <div className="filter_type">
                        <h5>Nombre de secret</h5>
                        {NB_OF_SECRET_PER_ROOMS[sizeRoom-1].map((element)=>{
                            return (
                                <label key={element}>{element}
                                    <input type="radio" value={element} checked={nbSecret == element} onChange={this.handleFilterNbSecretChange}/>
                                </label>
                            );
                        })}
                    </div>
                </form>
            </div>
        </>
 
    }
}

class FilterableSecrets extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            sizeRoom: 1,
            nbSecret: 1
        }
        this.handleFilterSizeRoomChange = this.handleFilterSizeRoomChange.bind(this)
        this.handleFilterNbSecretChange = this.handleFilterNbSecretChange.bind(this)
    }

    handleFilterSizeRoomChange(sizeRoom){
        this.setState({sizeRoom})  
        this.handleFilterNbSecretChange(NB_OF_SECRET_PER_ROOMS[sizeRoom-1][0]) 
    }

    handleFilterNbSecretChange(nbSecret){
        this.setState({nbSecret})
    }

    render(){
        const {secrets} = this.props
        return <React.Fragment>
            <SecretSearchBar sizeRoom={this.state.sizeRoom} nbSecret={this.state.nbSecret} onFilterSizeRoomChange={this.handleFilterSizeRoomChange} onFilterNbSecretChange={this.handleFilterNbSecretChange}/>
            <SecretTable secrets={secrets} sizeRoom={this.state.sizeRoom} nbSecret={this.state.nbSecret}/>
        </React.Fragment>  
    }
}

export default function Secret() {
    return (
        <>
            <h1 className='secret_header'>SECRET</h1>
            <FilterableSecrets secrets={ROOMS_SECRETS}></FilterableSecrets>
        </>
    );
}